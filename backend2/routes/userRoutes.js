const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);

router.post("/checkEmail", userControllers.checkEmailExists);

router.post("/register", userControllers.registerUser);

router.get("/allusers", userControllers.getAllUsers);

router.post("/login", userControllers.loginUser);

router.get("/details", auth.verify, userControllers.getProfile);

router.post("/enroll", auth.verify, userControllers.enroll);

router.get("/enrollments", auth.verify, userControllers.getAllEnrollments);

module.exports = router;