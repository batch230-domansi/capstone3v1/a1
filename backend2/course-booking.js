user {

			id - unique identifier for the document,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin,
			enrollments: [
				{

					id - document identifier,
					courseId - the unique identifier for the course,
					courseName - optional,
					isPaid,
					dateEnrolled
				}
			]

		}


		course {

			id - unique for the document (auto generated)
			name,
			description,
			price,
			slots,
			isActive,
			createdOn, 
			enrollees: [

				{
					id - document identifier (auto generated),
					userId,
					email,
					isPaid,
					dateEnrolled
				}
			]

		}


		user {

			id - unique identifier for the document,
			firstName,
			lastName,
			email,
			password,
			mobileNumber,
			isAdmin

		}

		course {

			id - unique for the document
			name,
			description,
			price,
			slots,
			isActive

		}

		enrollment {

			id - document identifier,
			userId - the unique identifier for the user,
			courseId - the unique identifier for the course,
			courseName - optional,
			isPaid,
			dateEnrolled

		}