const Course = require("../models/Course");
const auth = require("../auth");


module.exports.addCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	
	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
		slots: req.body.slots
	});

	if(req.userData.isAdmin){

		return newCourse.save()

		.then(course => {
			console.log(course);
			res.send(true)
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else {
		return res.status(401).send("You don't have access to this page!");
	};

};

module.exports.getAllCourses = (req, res) =>{
	

	if(req.userData.isAdmin){
		return Course.find({}).then(result => res.send(result));
	}
	else{
		return res.send(false);
	}
}

module.exports.getAllActive = (req, res) =>{
	return Course.find({isActive: true}).then(result => res.send(result));
}
module.exports.getCourse = (req, res) =>{
	console.log(req.params.courseId);

	return Course.findById(req.params.courseId).then(result => res.send(result));
}

module.exports.updateCourse = (req, res) =>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		}

		return Course.findByIdAndUpdate(req.params.courseId, updateCourse, {new:true})
		.then(result =>{
			console.log(result);
			res.send(result);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		});
	}
	else{
		return res.status(401).send("You don't have access to this page!");
	}
}


module.exports.archiveCourse = (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let updateIsActiveField = {
		isActive: req.body.isActive
	}

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(req.params.courseId, updateIsActiveField)
		.then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
	}
	else{
		return res.send(false);
	}
}
