const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.getAllUsers = (req, res) =>{
	
	
	return User.find({}).then(result => res.send(result));
	
	
}


module.exports.checkEmailExists = (req, res) =>{
	return User.find({email: req.body.email}).then(result =>{


		console.log(result);


		if(result.length > 0){
			return res.send(true);

		}

		else{
			return	res.send(false);

		}
	})
	.catch(error => res.send(error));
}


module.exports.registerUser = (req, res) =>{

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNumber: req.body.mobileNumber
	})

	console.log(newUser);

	return newUser.save()
	.then(user => {
		console.log(user);

		res.send(true);
	})
	.catch(error =>{
		console.log(error);
		res.send(false);
	})
}


module.exports.loginUser = (req, res) =>{
	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			return res.send(false);
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			if(isPasswordCorrect){
				
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else{
				return res.send(false);				
			}
		}
	})
}


module.exports.getProfile = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result =>{
		result.password = "***";
		res.send(result);
	})
}

module.exports.enroll = async (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let courseName = await Course.findById(req.body.courseId).then(result => result.name);

	let data = {
		userId: userData.id,
		email: userData.email,
		mobileNumber: userData.mobileNumber,
		courseId: req.body.courseId,
		courseName: courseName
	}

	console.log("data courseName:");
	console.log(data.courseName);
	console.log(data.email)

	let isUserUpdated = await User.findById(data.userId)
	.then(user =>{
		user.enrollments.push({
			courseId: data.courseId,
			courseName: data.courseName,
			mobileNumber: data.mobileNumber
		})

		return user.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(data.courseId).then(course =>{

		course.enrollees.push({
			userId: data.userId,
			email: data.email
		})
		console.log(data.email)

		course.slots -= 1;

		return course.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error =>{
			console.log(error);
			return false;
		})
	})

	console.log(isCourseUpdated);
	(isUserUpdated && isCourseUpdated) ? res.send(true) : res.send(false)

}

module.exports.getAllEnrollments = (req, res) => {
  return User.find({}, { enrollments: 1, _id: 0 })
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};