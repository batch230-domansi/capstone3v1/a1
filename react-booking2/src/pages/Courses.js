import { Fragment } from 'react';
import { Navigate } from "react-router-dom";
import { useEffect, useState, useContext } from "react";
import CourseCard from '../components/CourseCard';
import UserContext from '../UserContext';
import { Container, Row, Col } from 'react-bootstrap';

export default function Courses() {
  const { user } = useContext(UserContext);

  const [courses, setCourses] = useState([]);

  useEffect(() =>{
    // Will retrieve all the active courses
    fetch(`${process.env.REACT_APP_API_URL}/courses/`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setCourses(data.map(course =>{
        return(
          <CourseCard key={course._id} courseProp={course}/>
        );
      }));
    })
  }, []);



  return (
    user.isAdmin ? (
      <Navigate to="/admin" />
    ) : (
      <>
        <Container>
          <Row>
            <Col xs={12} md={{ span: 6, offset: 3 }} className="text-center">
              <h1>Offered Program</h1>
            </Col>
          </Row>
          <Row>{courses}</Row>
        </Container>
      </>
    )
  );
}
