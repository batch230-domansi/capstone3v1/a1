import {useContext, useEffect, useState} from "react";
import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));
	const [allCourses, setAllCourses] = useState([]);
	const [courseId, setCourseId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
  const [slots, setSlots] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const openAdd = () => setShowAdd(true); 
	const closeAdd = () => setShowAdd(false);
	const [showEdit, setShowEdit,setEnrollments] = useState(false);
	const [showEnrollments, setShowEnrollments] = useState(false);
	const openEnrollment = () => setShowEnrollments(true); 
	const closeEnrollment = () => setShowEnrollments(false);
	const [enrollments] = useState([]);


	const openEdit = (id) => {
		setCourseId(id);

		fetch(`${ process.env.REACT_APP_API_URL }/courses/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setSlots(data.slots);
		});

		setShowEdit(true)
	};


	const closeEdit = () => {

	    setName('');
	    setDescription('');
	    setPrice(0);
	    setSlots(0);
		setShowEdit(false);
	};



	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllCourses(data.map(course => {
				return (
					<tr key={course._id}>
						<td>{course._id}</td>
						<td>{course.name}</td>
						<td>{course.description}</td>
						<td>{course.price}</td>
						<td>{course.slots}</td>
						<td>{course.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								(course.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(course._id, course.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(course._id, course.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(course._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}

	useEffect(()=>{
		fetchData();
		fetchEnrollments();
	}, [])


	const archive = (id, courseName) =>{
		console.log(id);
		console.log(courseName);
		fetch(`${process.env.REACT_APP_API_URL}/courses/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${courseName} is now inactive.`
				});
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const unarchive = (id, courseName) =>{
		console.log(id);
		console.log(courseName);
		fetch(`${process.env.REACT_APP_API_URL}/courses/archive/${id}`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${courseName} is now active.`
				});
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const addCourse = (e) =>{
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/courses/addCourse`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    slots: slots
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Course succesfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});
		    		fetchData();
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setSlots(0);
	}



	const editCourse = (e) =>{
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    slots: slots
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Course succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});
		    		fetchData();
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })
		    setName('');
		    setDescription('');
		    setPrice(0);
		    setSlots(0);
	} 

	useEffect(() => {

        if(name != "" && description != "" && price > 0 && slots > 0){
            setIsActive(true);
        } 
        else {
            setIsActive(false);
        }

	}, [name, description, price, slots]);

	const fetchEnrollments = () => {
  fetch(`${process.env.REACT_APP_API_URL}/enrollments`, {
    headers: {
      "Authorization": `Bearer ${localStorage.getItem("token")}`,
    },
  })
  .then((res) => res.json())
  .then((data) => {
    // Update the enrollments state variable with the fetched data
    setEnrollments(data);
  })
  .catch((error) => {
    console.error("Error fetching enrollments:", error);
  });
};

// Call fetchEnrollments when the "Show Enrollments" button is clicked
const openEnrollments = () => {
  setShowEnrollments(true);
  fetchEnrollments();
};

return(
		(userRole)?
		<>
			{}
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Course</Button>
				<Button variant="success" className="mx-2" onClick={openEnrollment}>
					  Show Enrollments
					</Button>
			</div>
			{}
			{}
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Course ID</th>
		          <th>Course Name</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Slots</th>
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allCourses}
		      </tbody>
		    </Table>
			{}


	    	{}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addCourse(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Course</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Course Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Course Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>
	    	            <Form.Group controlId="description" className="mb-3">
	    	                <Form.Label>Course Description</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter Course Description" 
	    		                value = {description}
	    		                onChange={e => setDescription(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="price" className="mb-3">
	    	                <Form.Label>Course Price</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Course Price" 
	    		                value = {price}
	    		                onChange={e => setPrice(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	            <Form.Group controlId="slots" className="mb-3">
	    	                <Form.Label>Course Slots</Form.Label>
	    	                <Form.Control 
	    		                type="number" 
	    		                placeholder="Enter Course Slots" 
	    		                value = {slots}
	    		                onChange={e => setSlots(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    			</Modal.Body>
	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding course*/}

					{}
					<Modal show={showEnrollments} fullscreen={true} onHide={closeEnrollment}>
					  <Modal.Header closeButton>
					    <Modal.Title>Enrollment status</Modal.Title>
					  </Modal.Header>

					  <Modal.Body>
					    <Table striped bordered hover>
					      <thead>
					        <tr>
					          <th>Student ID</th>
					          <th>Student Name</th>
					          <th>Course Name</th>
					          <th>Amount to Pay</th>
					        </tr>
					      </thead>
					      <tbody>
					        {enrollments.map((enrollment) => (
					          <tr key={enrollment._id}>
					            <td>{enrollment.studentId}</td>
					            <td>{enrollment.studentName}</td>
					            <td>{enrollment.courseName}</td>
					            <td>{enrollment.amountToPay}</td>
					          </tr>
					        ))}
					      </tbody>
					    </Table>
					  </Modal.Body>
					  <Modal.Footer>
					    <Button variant="secondary" onClick={closeEnrollment}>
					      Close
					    </Button>
					  </Modal.Footer>
					</Modal>
					{}
    	{/*Modal for Editing a course*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => editCourse(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Course</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Course Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Course Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="description" className="mb-3">
    	                <Form.Label>Course Description</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter Course Description" 
    		                value = {description}
    		                onChange={e => setDescription(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="price" className="mb-3">
    	                <Form.Label>Course Price</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Course Price" 
    		                value = {price}
    		                onChange={e => setPrice(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="slots" className="mb-3">
    	                <Form.Label>Course Slots</Form.Label>
    	                <Form.Control 
    		                type="number" 
    		                placeholder="Enter Course Slots" 
    		                value = {slots}
    		                onChange={e => setSlots(e.target.value)}
    		                required
    	                />
    	            </Form.Group>
             </Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

	    	</Form>	
	    </Modal>
    </>	
		:

		<Navigate to="/courses" />
	)

	
}