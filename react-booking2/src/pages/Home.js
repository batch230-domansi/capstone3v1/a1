import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights' ;



export default function Home() {

	const data = {
		title: "Welcome to FluentMe",
		content: "Unlock the joy of learning with our engaging educational programs. We'll show you that studying can be a blast and knowledge is the ultimate superpower!",
		destination: "/",
		newLabel: "Level up your English! Click to enroll and start your language adventure today.",
		label: "Enroll now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}