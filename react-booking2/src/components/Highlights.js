import { Row, Col, Card } from 'react-bootstrap';

// export a function
export default function Highlights() {
    return (
    	

        <Row className="mt-3 mb-3">
        <div></div>
            <Col xs={12} md={3}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Why Choose FluentMe?</h2>
                        </Card.Title>
                        <Card.Text>
                               Our engaging and interactive lessons make learning fun, while our expert instructors are dedicated to your success. With personalized feedback and support, you'll reach your goals in no time. Plus, with a global community of learners to practice your English with, you'll have the opportunity to apply your new language skills in the real world. Don't just learn English - become fluent with FluentMe!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Our Approach</h2>
                        </Card.Title>
                        <Card.Text>
                            <p>At FluentMe, we believe that language learning should be:</p>

                            <p>Fun: Our lessons incorporate games, videos, and interactive activities to make learning enjoyable and engaging.</p>
                           <p>Practical: We use real-life situations and scenarios to help you apply your new language skills in the real world.</p>
                           <p>Personalized: Our instructors provide individualized feedback and support to help you overcome your challenges and achieve your goals.</p>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Testimonials</h2>
                        </Card.Title>
                        <Card.Text>
                            <p>Here's what some of our students have to say about FluentMe:</p>

                            <p>"As a business owner, I needed to improve my English skills for work. FluentMe's Business English program was exactly what I needed - it was practical, relevant, and helped me achieve my professional goals." - Hao, China</p>
                           <p>"I had always struggled with learning English until I found FluentMe. The instructors are amazing, and the lessons are so much fun that I forget I'm even studying!" - Maria, Japan</p>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            At FluentMe, you'll join a global community of learners from all over the world. You'll have the opportunity to practice your English with other students, participate in language exchange programs, and engage with our social media channels.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
